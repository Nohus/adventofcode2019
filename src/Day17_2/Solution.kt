package Day17_2

import Day17_2.Direction.*
import intcode.IntCodeComputer
import intcode.Num
import solveInputs

typealias Path = List<Pair<Char, Int>>

enum class Direction {
    NORTH, SOUTH, WEST, EAST;

    fun other() = when (this) {
        NORTH -> SOUTH
        SOUTH -> NORTH
        WEST -> EAST
        EAST -> WEST
    }

    fun getTurn(direction: Direction) = when {
        this == NORTH && direction == EAST -> 'R'
        this == EAST && direction == SOUTH -> 'R'
        this == SOUTH && direction == WEST -> 'R'
        this == WEST && direction == NORTH -> 'R'
        this == NORTH && direction == WEST -> 'L'
        this == WEST && direction == SOUTH -> 'L'
        this == SOUTH && direction == EAST -> 'L'
        this == EAST && direction == NORTH -> 'L'
        else -> throw IllegalArgumentException()
    }
}

data class Point(val x: Int, val y: Int) {

    fun move(direction: Direction) = when (direction) {
        NORTH -> Point(x, y - 1)
        SOUTH -> Point(x, y + 1)
        WEST -> Point(x - 1, y)
        EAST -> Point(x + 1, y)
    }
}

fun main() {
    solveInputs { input ->
        val path = getTurnPath(input)
        val segments = getSegmentsOfTurnPath(path)
        val main = getMainRoutine(path, segments)
        val functions = segments.map { it.joinToString(",") { "${it.first},${it.second}" } }

        IntCodeComputer(input)
            .setMemory(0, 2)
            .write("$main\n").apply {
                functions.forEach { write("$it\n") }
            }
            .write("n\n")
            .run().readAll().last()
    }
}

private fun getMainRoutine(path: Path, segments: List<Path>): String {
    return listOf(
        'A' to path.indicesOf(segments[0]),
        'B' to path.indicesOf(segments[1]),
        'C' to path.indicesOf(segments[2])
    )
        .asSequence()
        .map { segment -> segment.second.map { segment.first to it } }
        .flatten()
        .sortedBy { it.second }
        .map { it.first }
        .joinToString(",")
}

private fun getSegmentsOfTurnPath(path: Path): List<Path> {
    val segments = mutableListOf<Path>()
    var remaining = path.toList()
    while (remaining.isNotEmpty()) {
        val segment = getRepeatingSegment(remaining, segments.size == 2)
        segments += segment
        while (remaining.indexOf(segment) != -1) {
            val index = remaining.indexOf(segment)
            remaining = remaining.slice(remaining.indices - segment.indices.map { it + index })
        }
    }
    return segments
}

private fun getRepeatingSegment(path: Path, consumeAll: Boolean): Path {
    var segmentLength = 1
    var segment = path.subList(0, 1)
    while (path.isRepeating(segment) && segment.textLength() <= 20) {
        segmentLength++
        segment = path.subList(0, segmentLength)
        if (consumeAll && path.isRepetitionOf(segment)) return segment
    }
    return segment.dropLast(1)
}

private fun Path.isRepetitionOf(list: Path): Boolean {
    var build = list
    while (build.size < size) build = build + list
    return build == this
}

private fun Path.textLength(): Int {
    return joinToString(",") { "${it.first},${it.second}" }.length
}

private fun <T> List<T>.indexOf(list: List<T>): Int {
    return indicesOf(list).firstOrNull() ?: -1
}

private fun <T> List<T>.indicesOf(list: List<T>): List<Int> {
    val foundIndices = mutableListOf<Int>()
    var i = 0
    outer@while (i in indices) {
        for (j in list.indices) {
            if (this[i + j] != list[j]) {
                i++
                continue@outer
            }
        }
        foundIndices += i
        i += list.size
    }
    return foundIndices
}

private fun <T> List<T>.isRepeating(list: List<T>): Boolean {
    var found = false
    var i = 0
    outer@while (i in indices) {
        for (j in list.indices) {
            if (this.getOrNull(i + j) != list[j]) {
                i++
                continue@outer
            }
        }
        i += list.size
        if (!found) found = true
        else return true
    }
    return false
}

private fun getTurnPath(input: String): Path {
    val map = getMap(input)
    val path = getPath(map)
    val compressedPath = getCompressedPath(path)

    var direction = NORTH
    return compressedPath.map { pair ->
        (direction.getTurn(pair.first) to pair.second).also { direction = pair.first }
    }
}

private fun getMap(input: String): Map<Point, Char> {
    val view = IntCodeComputer(input).run().readAll().map(Num::toInt).map(Int::toChar).joinToString("")
    return mutableMapOf<Point, Char>().apply {
        view.lines().forEachIndexed { y, row ->
            row.forEachIndexed { x, char ->
                this[Point(x, y)] = char
            }
        }
    }
}

private fun getPath(map: Map<Point, Char>): List<Direction> {
    var position = map.entries.first { it.value == '^' }.key
    var direction = NORTH
    val path = mutableListOf<Direction>()
    while (true) {
        direction = getNextDirection(position, direction, map) ?: break
        path.add(direction)
        position = position.move(direction)
    }
    return path
}

private fun getNextDirection(position: Point, direction: Direction, map: Map<Point, Char>): Direction? {
    return if (map[position.move(direction)] == '#') return direction
    else Direction.values().filterNot { it == direction || it == direction.other() }.firstOrNull { map[position.move(it)] == '#' }
}

private fun getCompressedPath(originalPath: List<Direction>): List<Pair<Direction, Int>> {
    var path = originalPath.toList()
    val compressedPath = mutableListOf<Pair<Direction, Int>>()
    while (path.isNotEmpty()) {
        val segment = path.takeWhile { it == path[0] }
        path = path.subList(segment.size, path.size)
        compressedPath += segment.first() to segment.size
    }
    return compressedPath
}
