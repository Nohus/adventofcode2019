package Day20_1

import Day20_1.Direction.*
import Day20_1.Tile.*
import solveInputs

data class Point(val x: Int, val y: Int) {

    fun move(direction: Direction) = when (direction) {
        NORTH -> Point(x, y - 1)
        SOUTH -> Point(x, y + 1)
        WEST -> Point(x - 1, y)
        EAST -> Point(x + 1, y)
    }
}

enum class Direction {
    NORTH, SOUTH, WEST, EAST
}

sealed class Tile(val walkable: Boolean) {
    object Wall : Tile(false)
    data class Floor(val label: String? = null) : Tile(true)
    data class Label(val character: Char) : Tile(false)
    data class Portal(val destination: Point) : Tile(true)
}

fun main() {
    solveInputs { input ->
        val map = generateMap(input)
        val start = getLabeledPoint(map, "AA")
        val end = getLabeledPoint(map, "ZZ")
        val paths = dijkstra(map, start)
        paths[end]
    }
}

private fun getLabeledPoint(map: Map<Point, Tile>, label: String): Point {
    return map.entries
        .first { it.value is Floor && (it.value as Floor).label == label }
        .key
}

private fun dijkstra(map: Map<Point, Tile>, start: Point): Map<Point, Int> {
    val distances = mutableMapOf<Point, Int>().withDefault { Int.MAX_VALUE - 1 }
    distances[start] = 0
    val queue = map.keys.toMutableSet()
    while (queue.isNotEmpty()) {
        val point = queue.minBy { distances.getValue(it) }!!
        queue.remove(point)
        val neighbors = getNeighbors(map, point)
        neighbors.forEach {
            val distance = distances.getValue(point) + 1
            if (distance < distances.getValue(it)) {
                distances[it] = distance
            }
        }
    }
    return distances
}

private fun getNeighbors(map: Map<Point, Tile>, point: Point): List<Point> {
    val tile = map.getValue(point)
    val neighbors = Direction.values()
        .map { point.move(it) }
        .filter { map.getValue(it).walkable }
        .toMutableList()
    if (tile is Portal) neighbors += tile.destination
    return neighbors
}

private fun generateMap(input: String): Map<Point, Tile> {
    val map = mutableMapOf<Point, Tile>()
    input.lines().forEachIndexed { y, row ->
        row.forEachIndexed { x, char ->
            when (char) {
                '#' -> Wall
                '.' -> Floor()
                in 'A'..'Z' -> Label(char)
                else -> null
            }?.let { map[Point(x, y)] = it }
        }
    }
    return mapPortals(mapLabels(map)).withDefault { Wall }
}

private fun mapPortals(map: Map<Point, Tile>): Map<Point, Tile> {
    val portalMap = map.toMutableMap()
    val labeledFloor = map
        .filterValues { it is Floor && it.label != null }
        .map { it.key to it.value as Floor }
        .filterNot { it.second.label == "AA" || it.second.label == "ZZ" }
    labeledFloor.forEach { floor ->
        val other = labeledFloor.first { it.second.label == floor.second.label && it.first != floor.first }.first
        portalMap[floor.first] = Portal(other)
    }
    return portalMap
}

private fun mapLabels(map: Map<Point, Tile>): Map<Point, Tile> {
    val labeledMap = map.toMutableMap()
    map.filterValues { it is Floor }.forEach {
        val label = findLabel(map, it.key)
        if (label != null) labeledMap[it.key] = Floor(label)
    }
    return labeledMap
}

private fun findLabel(map: Map<Point, Tile>, point: Point): String? {
    val direction = Direction.values().firstOrNull {
        map[point.move(it)] is Label
    } ?: return null
    val labelPoint = point.move(direction)
    val letter = map[labelPoint] as Label
    val letter2 = map[labelPoint.move(direction)] as Label
    val label = "${letter.character}${letter2.character}"
    return if (direction in listOf(SOUTH, EAST)) label
    else label.reversed()
}
