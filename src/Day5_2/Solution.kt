package Day5_2

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        IntCodeComputer(input, 5).run().readAll().last()
    }
}
