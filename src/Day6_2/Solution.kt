package Day6_2

import solveInputs

data class Body(
    val name: String,
    val bodies: MutableList<Body>,
    val parentName: String?
) {

    val parent by lazy { bodies.firstOrNull { it.name == parentName } }

    fun getParents(): List<Body> = parent?.let { listOf(it) + it.getParents() } ?: emptyList()
}

fun main() {
    solveInputs { input ->
        val orbits = input.lines().map { line ->
            line.split(")").let { it[0] to it[1] }
        }
        val bodies = mutableListOf<Body>()
        bodies += orbits.map { Body(it.second, bodies, it.first) }
        val allChildren = orbits.map { it.second }
        val centerOfMass = orbits.map { it.first }.first { it !in allChildren }
        bodies += Body(centerOfMass, bodies, null)

        val youParents = bodies.first { it.name == "YOU" }.getParents()
        val santaParents = bodies.first { it.name == "SAN" }.getParents()
        val commonParent = youParents.first { it in santaParents }
        youParents.indexOf(commonParent) + santaParents.indexOf(commonParent)
    }
}
