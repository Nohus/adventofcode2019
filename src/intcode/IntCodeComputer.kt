package intcode

import intcode.ArgumentMode.*
import intcode.StopCondition.*

class IntCodeComputer(
    memoryInput: Any,
    private val initialInput: Int? = null,
    private val initialInputs: List<Int> = emptyList(),
    private val debug: Boolean = false,
    private val name: Any? = null
) {

    private val Argument.info: String get() {
        return when (mode) {
            POSITION -> "[$argument]${memory[argument]}"
            IMMEDIATE -> "$argument"
            RELATIVE -> "[$relativeBase+$argument]${memory[relativeBase + argument]}"
        }
    }

    private var Argument.value: Num get() {
        return when (mode) {
            POSITION -> memory[argument]
            IMMEDIATE -> argument
            RELATIVE -> memory[relativeBase + argument]
        }
    }
    set(value) {
        when (mode) {
            POSITION -> memory[argument] = value
            IMMEDIATE -> throw IntCodeException()
            RELATIVE -> memory[relativeBase + argument] = value
        }
    }

    private val instructions = listOf(
        Instruction("ADD", 1, 3, {
            it[2].value = it[0].value + it[1].value
        }) {
            "[${it[2].argument}] = ${it[0].info} + ${it[1].info} (${it[0].value + it[1].value})"
        },
        Instruction("MULTIPLY", 2, 3, {
            it[2].value = it[0].value * it[1].value
        }) {
            "[${it[2].argument}] = ${it[0].info} * ${it[1].info} (${it[0].value * it[1].value})"
        },
        Instruction("INPUT", 3, 1, {
            if (inputBuffer.isEmpty()) throw IOError("Missing input")
            it[0].value = inputBuffer.removeAt(0)
        }) {
            "[${it[0].argument}] = ${inputBuffer[0]}"
        },
        Instruction("OUTPUT", 4, 1, {
            outputBuffer += it[0].value
        }) {
            it[0].info
        },
        Instruction("JUMP IF TRUE", 5, 2, {
            if (it[0].value.isTrue()) programCounter = it[1].value
        }) {
            return@Instruction if (it[0].value != Num.ZERO) {
                "Jump to ${it[1].info} (${it[0].info} == true)"
            } else {
                "Skipping jump to ${it[1].info} (${it[0].info} != true)"
            }
        },
        Instruction("JUMP IF FALSE", 6, 2, {
            if (it[0].value.isFalse()) programCounter = it[1].value
        }) {
            return@Instruction if (it[0].value.isFalse()) {
                "Jump to ${it[1].info} (${it[0].info} == false)"
            } else {
                "Skipping jump to ${it[1].info} (${it[0].info} != false)"
            }
        },
        Instruction("LESS THAN", 7, 3, {
            it[2].value = if (it[0].value < it[1].value) Num.ONE else Num.ZERO
        }) {
            "[${it[2].argument}] = ${it[0].info} < ${it[1].info}"
        },
        Instruction("EQUALS", 8, 3, {
            it[2].value = if (it[0].value == it[1].value) Num.ONE else Num.ZERO
        }) {
            "[${it[2].argument}] = ${it[0].info} == ${it[1].info}"
        },
        Instruction("RELATIVE BASE", 9, 1, {
            relativeBase += it[0].value
        }) {
            "RELATIVE BASE = ${relativeBase + it[0].value} (${relativeBase} + ${it[0].info})"
        },
        Instruction("HALT", 99, 0, {
            throw HaltOpcode()
        }) {
            "Halt!"
        }
    )

    private val originalMemory: List<Num>
    private var memory = Memory()
    private var programCounter: Num = Num.ZERO
    private var relativeBase: Num = Num.ZERO
    private var inputBuffer = mutableListOf<Num>()
    private val outputBuffer = mutableListOf<Num>()

    init {
        @Suppress("UNCHECKED_CAST")
        originalMemory = when (memoryInput) {
            is List<*> -> (memoryInput as List<Num>)
            is String -> memoryInput.split(",").map { Num(it) }
            else -> throw IllegalArgumentException()
        }
        reset()
    }

    fun reset() = apply {
        memory = Memory(originalMemory)
        programCounter = Num.ZERO
        relativeBase = Num.ZERO

        inputBuffer.clear()
        outputBuffer.clear()
        initialInput?.let { inputBuffer.add(it.toNum()) }
        inputBuffer.addAll(initialInputs.map {it.toNum() })
    }

    operator fun set(address: Long, value: Int) {
        memory[address] = value.toNum()
    }

    operator fun get(address: Long) = memory[address]

    fun setMemory(address: Long, value: Int) = apply {
        this[address] = value
    }

    fun write(vararg input: Num) = apply {
        inputBuffer.addAll(input)
    }

    fun write(vararg input: Int) = apply {
        inputBuffer.addAll(input.map(Int::toNum))
    }

    fun write(text: String) = apply {
        inputBuffer.addAll(text.map(Char::toInt).map(Int::toNum))
    }

    fun clearInput() = apply {
        inputBuffer.clear()
    }

    fun read(): Int? {
        if (outputBuffer.isEmpty()) run(OUTPUT_AVAILABLE)
        return if (outputBuffer.isNotEmpty()) outputBuffer.removeAt(0).toInt() else null
    }

    fun readNum(): Num? {
        if (outputBuffer.isEmpty()) run(OUTPUT_AVAILABLE)
        return if (outputBuffer.isNotEmpty()) outputBuffer.removeAt(0) else null
    }

    fun readAll(): List<Num> {
        return outputBuffer.toList().also { outputBuffer.clear() }
    }

    fun readText(): String {
        val textBuffer = mutableListOf<Char>()
        while (true) {
            textBuffer.addAll(outputBuffer.map(Num::toInt).map {
                if (it >= 256) it.toString().toCharArray().toList()
                else listOf(it.toChar())
            }.flatten())
            outputBuffer.clear()
            if (textBuffer.lastOrNull() == '\n') break
            run(OUTPUT_AVAILABLE)
        }
        return textBuffer.joinToString("").substring(0, textBuffer.lastIndex)
    }

    fun run(stopCondition: StopCondition = HALT) = apply {
        try {
            while (true) {
                checkStopCondition(stopCondition)
                execute()
            }
        } catch (interrupt: Interrupt) {}
    }

    private fun checkStopCondition(stopCondition: StopCondition) {
        when (stopCondition) {
            OUTPUT_AVAILABLE -> {
                if (outputBuffer.isNotEmpty()) {
                    throw OutputAvailable()
                }
            }
            INPUT_NEEDED -> {
                if (isNextInstructionInput() && inputBuffer.isEmpty()) {
                    throw InputNeeded()
                }
            }
            HALT -> {}
        }
    }

    private fun isNextInstructionInput() = memory[programCounter] % 100 == 3.toNum()

    private fun execute() {
        val decoded = decode(programCounter)
        val instruction = decoded.instruction
        val arguments = decoded.arguments

        programCounter += instruction.length

        if (debug) decoded.print()
        instruction.action(arguments)
    }

    private fun decode(pointer: Num): DecodedInstruction {
        val opcodeWithModes = memory[pointer]
        val opcode = opcodeWithModes % 100
        val instruction = instructions.firstOrNull() { it.opcodeNum == opcode } ?: throw InvalidOpcode()
        val modes = List(instruction.argumentCount) {
            when (getDigit(opcodeWithModes, it + 2)) {
                0 -> POSITION
                1 -> IMMEDIATE
                2 -> RELATIVE
                else -> throw InvalidOpcode()
            }
        }
        val argumentValues = memory.getRange(pointer + 1, instruction.argumentCount)
        val arguments = argumentValues.zip(modes).map { Argument(it.first, it.second) }
        return DecodedInstruction(instruction, arguments)
    }

    private fun getDigit(number: Num, position: Int): Int {
        val divisor = List(position) { 10 }.reduce { acc, value -> acc * value }
        return (number.toInt() / divisor) % 10
    }

    private fun DecodedInstruction.print() {
        val padLength = instructions.map { it.name.length }.max() ?: 0
        val prefix = name?.let { "[$it] " } ?: ""
        println("$prefix${instruction.name.padStart(padLength)} --> ${instruction.toString(arguments)}")
    }
}
