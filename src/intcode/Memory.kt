package intcode

class Memory(
    initialData: List<Num> = emptyList()
) {

    private val memory = mutableMapOf<Num, Num>()

    init {
        initialData.forEachIndexed { index, num ->
            set(index, num)
        }
    }

    operator fun get(address: Num): Num {
        return memory[address] ?: Num.ZERO
    }

    operator fun get(address: Long): Num {
        return get(address.toNum())
    }

    operator fun set(address: Num, value: Num) {
        memory[address] = value
    }

    operator fun set(address: Int, value: Num) {
        set(address.toNum(), value)
    }

    operator fun set(address: Long, value: Num) {
        set(address.toNum(), value)
    }

    fun getRange(address: Num, length: Int) = List(length) { this[address + it] }
}
