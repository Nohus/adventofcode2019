package intcode

enum class StopCondition {
    OUTPUT_AVAILABLE, INPUT_NEEDED, HALT
}
