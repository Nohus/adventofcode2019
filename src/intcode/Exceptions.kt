package intcode

open class IntCodeException(message: String = "") : Exception(message)
class InvalidOpcode : IntCodeException()
class IOError(message: String) : IntCodeException(message)

sealed class Interrupt : IntCodeException()
class HaltOpcode : Interrupt()
class OutputAvailable : Interrupt()
class InputNeeded : Interrupt()
