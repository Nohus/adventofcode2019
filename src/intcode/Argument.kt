package intcode

data class Argument(
    val argument: Num,
    val mode: ArgumentMode
)
