package intcode

data class Instruction(
    val name: String,
    val opcode: Int,
    val argumentCount: Int,
    val action: (List<Argument>) -> Unit,
    val toString: (List<Argument>) -> String
) {

    val length = 1 + argumentCount
    val opcodeNum by lazy { opcode.toNum() }
}
