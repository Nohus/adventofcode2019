package intcode

enum class ArgumentMode {
    POSITION, IMMEDIATE, RELATIVE
}
