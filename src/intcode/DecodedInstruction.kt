package intcode

data class DecodedInstruction(
    val instruction: Instruction,
    val arguments: List<Argument>
)
