package intcode

import java.math.BigInteger

typealias Num = BigInteger

fun String.toNum() = Num(this)

fun Long.toNum() = Num.valueOf(this)

fun Int.toNum() = Num.valueOf(this.toLong())

operator fun Num.unaryMinus() = negate()

operator fun Num.rem(other: Int) = mod(other.toNum())

operator fun Num.rem(other: Num) = mod(other)

operator fun Num.plus(other: Int) = add(other.toNum())

operator fun Num.plus(other: Num) = add(other)

operator fun Num.minus(other: Num) = subtract(other)

operator fun Num.times(other: Num) = multiply(other)

fun Num.isTrue() = !equals(0.toNum())

fun Num.isFalse() = equals(0.toNum())
