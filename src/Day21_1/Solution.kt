package Day21_1

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        IntCodeComputer(input).apply {
            readText()
            write("NOT A J\n") // Hole at 1 -> Jump
            write("NOT B T\n") // Hole at 2 -> Temp
            write("OR T J\n") // Hole at 2 or 1 -> Jump
            write("NOT C T\n") // Hole at 3 -> Temp
            write("OR T J\n") // Hole at 3 or 2 or 1 -> Jump
            write("AND D J\n") // Hole we can jump over -> Jump
            write("WALK\n")
            readText()
            readText()
            readText()
        }.readText()
    }
}
