package Day8_1

import solveInputs

fun main() {
    solveInputs { input ->
        val layers = input.chunked(1).chunked(25 * 6)
        val layer = layers.minBy { it.count { it == "0" } }!!
        layer.count { it == "1" } * layer.count { it == "2" }
    }
}
