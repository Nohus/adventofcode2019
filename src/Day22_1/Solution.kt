package Day22_1

import solveInputs

class Deck(size: Int) {

    var deck = List(size) { it }
        private set

    fun deal(){
        deck = deck.reversed()
    }

    fun cut(n: Int) {
        deck = if (n >= 0) {
            deck.subList(n, deck.size) + deck.subList(0, n)
        } else {
            deck.subList(deck.size + n, deck.size) + deck.subList(0, deck.size + n)
        }
    }

    fun deal(increment: Int) {
        val remaining = deck.toMutableList()
        val table = Array(deck.size) { -1 }
        var position = 0
        while (remaining.isNotEmpty()) {
            table[position] = remaining.removeAt(0)
            position += increment
            position %= deck.size
        }
        deck = table.toList()
    }
}

fun main() {
    solveInputs { input ->
        val deck = Deck(10007)
        input.lines().forEach {
            when {
                it.startsWith("deal into") -> deck.deal()
                it.startsWith("deal with") -> deck.deal(it.split(" ").last().toInt())
                it.startsWith("cut") -> deck.cut(it.split(" ").last().toInt())
            }
        }
        deck.deck.indexOf(2019)
    }
}
