package Day19_2

import intcode.IntCodeComputer
import solveInputs

data class Point(val x: Int, val y: Int)

fun main() {
    solveInputs { input ->
        val computer = IntCodeComputer(input)
        var point = Point(0, 0)
        while (true) {
            if (isFittingSquare(computer, point, 100)) {
                point.copy(y = point.y - 99).let { return@solveInputs it.x * 10000 + it.y }
            }
            point = getNextPoint(computer, point)
        }
    }
}

private fun isFittingSquare(computer: IntCodeComputer, point: Point, side: Int): Boolean {
    return isValid(computer, point.copy(x = point.x + side - 1, y = point.y - side + 1))
}

private fun isValid(computer: IntCodeComputer, point: Point): Boolean {
    return computer.reset().write(point.x, point.y).read()!!.toInt() == 1
}

private fun getNextPoint(computer: IntCodeComputer, point: Point): Point {
    val down = Point(point.x, point.y + 1)
    return if (isValid(computer, down)) down
    else Point(point.x + 1, point.y)
}
