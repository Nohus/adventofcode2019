package Day14_1

import solveInputs

data class Resource(
    val name: String,
    val amount: Long
)

data class Reaction(
    val inputs: List<Resource>,
    val output: Resource
)

private const val FUEL = "FUEL"
private const val ORE = "ORE"

fun main() {
    solveInputs { input ->
        val reactions = input.lines().map {
            val inputs = it.substringBefore(" =>").split(", ").map { parseResource(it) }
            val output = parseResource(it.substringAfter("=> "))
            Reaction(inputs, output)
        }

        var requirements = listOf(Resource(FUEL, 1))
        while (isOnlyOreRequired(requirements) == null) {
            val reaction = chooseReaction(reactions, requirements)
            requirements = applyReaction(reaction.first, reaction.second, requirements)
            requirements = mergeResources(requirements)
        }
        isOnlyOreRequired(requirements)
    }
}

private fun chooseReaction(reactions: List<Reaction>, requirements: List<Resource>): Pair<Reaction, Long> {
    reactions.firstOrNull { reaction ->
        requirements.any { reaction.output.name == it.name && reaction.output.amount <= it.amount }
    }?.let { reaction ->
        val requirement = requirements.first { it.name == reaction.output.name }
        return reaction to (requirement.amount / reaction.output.amount)
    }
    return reactions.first { it.output.name in requirements.filter { it.amount > 0 }.map { it.name } } to 1
}

private fun applyReaction(reaction: Reaction, times: Long, requirements: List<Resource>): List<Resource> {
    val input = reaction.inputs.map { Resource(it.name, it.amount * times) }
    val output = reaction.output.let { Resource(it.name, it.amount * times) }
    return requirements + input + Resource(output.name, -output.amount)
}

private fun mergeResources(resources: List<Resource>): List<Resource> {
    return resources
        .groupBy { it.name }
        .map { Resource(it.key, it.value.map { it.amount }.sum()) }
        .filterNot { it.amount == 0L }
}

private fun isOnlyOreRequired(requirements: List<Resource>): Long? {
    return if (requirements.filter { it.name != ORE }.all { it.amount <= 0 }) {
        requirements.first { it.name == ORE }.amount
    } else null
}

private fun parseResource(text: String): Resource {
    return text.split(" ").let { Resource(it[1], it[0].toLong()) }
}
