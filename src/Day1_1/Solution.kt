package Day1_1

import solveInputs

fun main() {
    solveInputs { input ->
        input.lines().map(String::toInt).map(::getFuelRequirement).sum()
    }
}

fun getFuelRequirement(mass: Int) = (mass / 3) - 2
