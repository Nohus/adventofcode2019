package Day22_2

import intcode.*
import solveInputs

fun main() {
    solveInputs { input ->
        // Inverse modulo solution found online and adapted
        var a = Num.ONE
        var b = Num.ZERO
        val deckSize = 119315717514047.toNum()
        input.lines().forEach {
            when {
                it.startsWith("deal into") -> {
                    // x -> -x - 1
                    // ax + b -> -ax - b - 1
                    // !b == -b - 1
                    a = -a % deckSize
                    b = !b % deckSize
                }
                it.startsWith("cut") -> {
                    // x -> x - i
                    // ax + b -> ax + b - i
                    val cut = it.split(' ').last().toNum()
                    b -= cut
                    b %= deckSize
                }
                it.startsWith("deal with") -> {
                    // x -> x * i
                    // ax + b -> aix + bi
                    val increment = it.split(' ').last().toNum()
                    a *= increment % deckSize
                    b *= increment % deckSize
                }
            }
        }
        a = a.modInverse(deckSize)
        b = (-b * a) % deckSize
        var c = Num.ONE
        var d = Num.ZERO
        var shufflesLeft = 101741582076661
        while (shufflesLeft > 0) {
            if (shufflesLeft % 2 != 0L) {
                // a(cx + d) + b == acx + (ad + b)
                c = a * c % deckSize
                d = (a * d + b) % deckSize
            }
            shufflesLeft = shufflesLeft shr 1
            b = (a * b + b) % deckSize
            a = a * a % deckSize
        }
        (2020.toNum() * c + d) % deckSize
    }
}
