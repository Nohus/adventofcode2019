package Day15_2

import Day15_2.Direction.*
import Day15_2.Status.*
import Day15_2.Tile.*
import intcode.IntCodeComputer
import solveInputs
import java.awt.Color
import java.awt.color.ColorSpace
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

enum class Direction {
    NORTH, SOUTH, WEST, EAST;

    fun toInt() = when (this) {
        NORTH -> 1
        SOUTH -> 2
        WEST -> 3
        EAST -> 4
    }

    fun other() = when (this) {
        NORTH -> SOUTH
        SOUTH -> NORTH
        WEST -> EAST
        EAST -> WEST
    }
}

data class Point(val x: Int, val y: Int) {

    fun move(direction: Direction) = when (direction) {
        NORTH -> Point(x, y - 1)
        SOUTH -> Point(x, y + 1)
        WEST -> Point(x - 1, y)
        EAST -> Point(x + 1, y)
    }
}

enum class Tile {
    UNKNOWN, FLOOR, WALL, OXYGEN
}

enum class Status {
    HIT, MOVED, REACHED;

    companion object {
        fun fromInt(status: Int) = when (status) {
            0 -> HIT
            1 -> MOVED
            2 -> REACHED
            else -> throw IllegalArgumentException()
        }
    }
}

data class Node(
    val position: Point,
    val previous: Node? = null,
    val directionBack: Direction? = null,
    val unexploredDirection: MutableList<Direction>
) {

    fun move(direction: Direction, map: Map<Point, Tile>): Node {
        return if (direction == directionBack) {
            previous!!
        } else {
            unexploredDirection -= direction
            val newPosition = position.move(direction)
            val directions = listOf(NORTH, SOUTH, EAST, WEST).filter {
                map[newPosition.move(it)] ?: UNKNOWN == UNKNOWN
            }
            Node(newPosition, this, direction.other(), directions.toMutableList())
        }
    }
}

fun main() {
    solveInputs { input ->
        val computer = IntCodeComputer(input)
        val map = mutableMapOf<Point, Tile>()
        map += Point(21, 21) to FLOOR
        var position = Point(21, 21)
        var node = Node(position, null, null, listOf(NORTH, SOUTH, EAST, WEST).toMutableList())

        while (true) {
            printMap(position, map, false)
            val direction: Direction = if (node.unexploredDirection.isNotEmpty()) {
                node.unexploredDirection.first()
            } else {
                node.directionBack ?: break
            }
            computer.write(direction.toInt())
            val status = Status.fromInt(computer.read()!!.toInt())
            val newPosition = position.move(direction)
            when (status) {
                HIT -> {
                    map[newPosition] = WALL
                    node.unexploredDirection -= direction
                }
                MOVED -> {
                    map[newPosition] = FLOOR
                    position = newPosition
                    node = node.move(direction, map)
                }
                REACHED -> {
                    map[newPosition] = OXYGEN
                    position = newPosition
                    node = node.move(direction, map)
                }
            }
        }

        var minutes = 0
        val oxygen = mutableListOf(map.entries.first { it.value == OXYGEN }.key)
        while (true) {
            printMap(position, map, true)
            val fill = oxygen.map { oxygenPosition ->
                listOf(NORTH, SOUTH, EAST, WEST)
                    .map { oxygenPosition.move(it) }
                    .filter { map[it] == FLOOR }
            }.flatten()
            oxygen.clear()
            if (fill.isEmpty()) break
            fill.forEach {
                map[it] = OXYGEN
                oxygen += it
            }
            minutes++
        }
        minutes
    }
}

private var frame = 1
private fun printMap(droid: Point, map: Map<Point, Tile>, mapped: Boolean) {
    val width = 41
    val height = 41
    val scale = 8
    val bitmap = BufferedImage(width * scale, height * scale, ColorSpace.TYPE_RGB)
    for (y in 0 until width) {
        for (x in 0 until height) {
            var color = when (map[Point(x, y)] ?: if (mapped) WALL else UNKNOWN) {
                UNKNOWN -> Color.decode("#212121")
                FLOOR -> Color.decode("#FAFAFA")
                WALL -> Color.decode("#d32f2f")
                OXYGEN -> Color.decode("#03A9F4")
            }
            if (!mapped && droid.x == x && droid.y == y) color = Color.decode("#4CAF50")
            for (pixelY in y * scale until y * scale + scale) {
                for (pixelX in x * scale until x * scale + scale) {
                    bitmap.setRGB(pixelX, pixelY, color.rgb)
                }
            }
        }
    }
    ImageIO.write(bitmap, "PNG", File("oxygen/map-$frame.png"))
    frame++
}
