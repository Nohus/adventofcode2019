package Day4_1

import solveInputs

fun main() {
    solveInputs { input ->
        val range = input.split("-").map(String::toInt).let { it[0]..it[1] }
        range.asSequence()
            .map(Int::toString)
            .filter(::hasAdjacentDigits)
            .filterNot(::hasDecreasingDigits)
            .count()
    }
}

private fun hasAdjacentDigits(password: String): Boolean {
    for (i in 0 until password.lastIndex) {
        if (password[i] == password[i + 1]) return true
    }
    return false
}

private fun hasDecreasingDigits(password: String): Boolean {
    for (i in 0 until password.lastIndex) {
        if (password[i + 1].toInt() < password[i].toInt()) return true
    }
    return false
}
