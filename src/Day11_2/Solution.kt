package Day11_2

import Day11_2.Direction.*
import intcode.IntCodeComputer
import solveInputs

data class Point(val x: Int, val y: Int) {

    fun move(direction: Direction): Point {
        return when (direction) {
            LEFT -> Point(x - 1, y)
            RIGHT -> Point(x + 1, y)
            UP -> Point(x, y - 1)
            DOWN -> Point(x, y + 1)
        }
    }
}

enum class Direction {
    LEFT, RIGHT, UP, DOWN;

    fun rotate(input: Int): Direction {
        return if (input == 0) {
            when (this) {
                LEFT -> DOWN
                DOWN -> RIGHT
                RIGHT -> UP
                UP -> LEFT
            }
        } else {
            when (this) {
                LEFT -> UP
                UP -> RIGHT
                RIGHT -> DOWN
                DOWN -> LEFT
            }
        }
    }
}

fun main() {
    solveInputs { input ->
        val computer = IntCodeComputer(input)
        val map = mutableMapOf<Point, Int>()
        map[Point(0, 0)] = 1
        var position = Point(0, 0)
        var direction = UP
        while (true) {
            computer.write(map[position] ?: 0)
            map[position] = computer.read() ?: break
            val turn = computer.read() ?: break
            direction = direction.rotate(turn)
            position = position.move(direction)
        }

        val minX = map.keys.map { it.x }.min()!!
        val minY = map.keys.map { it.y }.min()!!
        val maxX = map.keys.map { it.x }.max()!!
        val maxY = map.keys.map { it.y }.max()!!
        for (y in minY..maxY) {
            for (x in minX..maxX) {
                print(if (map[Point(x, y)] == 1) "█" else " ")
            }
            println()
        }
    }
}
