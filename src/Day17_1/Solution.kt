package Day17_1

import Day17_1.Direction.*
import intcode.IntCodeComputer
import intcode.Num
import solveInputs

enum class Direction {
    NORTH, SOUTH, WEST, EAST
}

data class Point(val x: Int, val y: Int) {

    fun move(direction: Direction) = when (direction) {
        NORTH -> Point(x, y - 1)
        SOUTH -> Point(x, y + 1)
        WEST -> Point(x - 1, y)
        EAST -> Point(x + 1, y)
    }
}

fun main() {
    solveInputs { input ->
        val computer = IntCodeComputer(input)
        val view = computer.run().readAll().map(Num::toInt).map(Int::toChar).joinToString("")
        val map = mutableMapOf<Point, Char>()
        view.lines().forEachIndexed { y, row ->
            row.forEachIndexed { x, char ->
                map[Point(x, y)] = char
            }
        }
        val intersections = map.entries.filter { it.value == '#' }.map { it.key }.filter { isIntersection(it, map) }
        intersections.map { it.x * it.y }.sum()
    }
}

private fun isIntersection(point: Point, map: Map<Point, Char>): Boolean {
    return Direction.values().map { point.move(it) }.all { map[it] == '#' }
}
