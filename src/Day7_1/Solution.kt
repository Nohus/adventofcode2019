package Day7_1

import intcode.IntCodeComputer
import intcode.Num
import intcode.toNum
import solveInputs

fun main() {
    solveInputs { input ->
        val phases = (0..4).toList()
        val amplifiers = List(phases.size) { IntCodeComputer(input) }
        getPermutations(phases).map {
            amplifiers.zip(it).fold(Num.ZERO) { current, attempt ->
                attempt.first.reset().write(attempt.second.toNum(), current).readNum()!!
            }
        }.max()
    }
}

private fun getPermutations(list: List<Int>): List<List<Int>> {
    return if (list.size == 2) listOf(listOf(list[0], list[1]), listOf(list[1], list[0]))
    else list.map { initial ->
        getPermutations(list - initial).map { permutation -> listOf(initial) + permutation }
    }.flatten()
}
