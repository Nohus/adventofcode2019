package Day20_2

import Day20_2.Direction.*
import Day20_2.Tile.*
import solveInputs

data class Point(val x: Int, val y: Int, val level: Int) {

    fun move(direction: Direction) = when (direction) {
        NORTH -> Point(x, y - 1, level)
        SOUTH -> Point(x, y + 1, level)
        WEST -> Point(x - 1, y, level)
        EAST -> Point(x + 1, y, level)
    }
}

enum class Direction {
    NORTH, SOUTH, WEST, EAST
}

sealed class Tile(val walkable: Boolean) {
    object Wall : Tile(false)
    class Floor(val label: String? = null, val isLabelInner: Boolean = false) : Tile(true)
    class Label(val character: Char) : Tile(false)
    class Portal(val destination: Point, val isInner: Boolean) : Tile(true)
}

fun main() {
    solveInputs { input ->
        val map = generateMap(input)
        val start = getLabeledPoint(map, "AA")
        bfs(map, start, "ZZ")
    }
}

private fun getLabeledPoint(map: Map<Point, Tile>, label: String): Point {
    return map.entries
        .first { it.value is Floor && (it.value as Floor).label == label }
        .key
}

private fun bfs(map: Map<Point, Tile>, start: Point, end: String): Int? {
    val distances = mutableMapOf<Point, Int>().withDefault { Int.MAX_VALUE - 1 }
    distances[start] = 0
    val unvisited = mutableListOf(start)
    val visited = mutableSetOf<Point>()

    while (unvisited.isNotEmpty()) {
        val point = unvisited.minBy { it.level }!!
        unvisited -= point
        visited += point

        if (map.getValue(point).let { it is Floor && it.label == end && point.level == 0 }) {
            return distances[point]!!
        }

        val neighbors = getNeighbors(map, point)
        unvisited += neighbors.filter { it !in visited }
        neighbors.forEach {
            val distance = distances.getValue(point) + 1
            if (distance < distances.getValue(it)) {
                distances[it] = distance
            }
        }
    }
    return null
}

private fun getNeighbors(map: Map<Point, Tile>, point: Point): List<Point> {
    val tile = map.getValue(point.copy(level = 0))
    val neighbors = Direction.values()
        .map { point.move(it) }
        .filter { map.getValue(it.copy(level = 0)).walkable }
        .filter {
            val portal = map.getValue(it.copy(level = 0))
            if (portal is Portal && point.level == 0 && !portal.isInner) return@filter false // Outer portals don't work at level 0
            return@filter true
        }
        .toMutableList()
    if (tile is Portal) {
        neighbors += if (tile.isInner) tile.destination.copy(level = point.level + 1)
        else tile.destination.copy(level = point.level - 1)
    }
    return neighbors
}

private fun generateMap(input: String): Map<Point, Tile> {
    val map = mutableMapOf<Point, Tile>()
    input.lines().forEachIndexed { y, row ->
        row.forEachIndexed { x, char ->
            when (char) {
                '#' -> Wall
                '.' -> Floor()
                in 'A'..'Z' -> Label(char)
                else -> null
            }?.let { map[Point(x, y, 0)] = it }
        }
    }
    return mapPortals(mapLabels(map)).withDefault { Wall }
}

private fun mapPortals(map: Map<Point, Tile>): Map<Point, Tile> {
    val portalMap = map.toMutableMap()
    val labeledFloor = map
        .filterValues { it is Floor && it.label != null }
        .map { it.key to it.value as Floor }
        .filterNot { it.second.label == "AA" || it.second.label == "ZZ" }
    labeledFloor.forEach { floor ->
        val other = labeledFloor.first { it.second.label == floor.second.label && it.first != floor.first }.first
        portalMap[floor.first] = Portal(other, floor.second.isLabelInner)
    }
    return portalMap
}

private fun isInner(map: Map<Point, Tile>, point: Point, direction: Direction): Boolean {
    val verticalDivider = map.keys.map { it.x }.let { it.max()!! - it.min()!! } / 2
    val horizontalDivider = map.keys.map { it.y }.let { it.max()!! - it.min()!! } / 2
    return point.x < verticalDivider && direction == EAST
        || point.x > verticalDivider && direction == WEST
        || point.y < horizontalDivider && direction == SOUTH
        || point.y > horizontalDivider && direction == NORTH
}

private fun mapLabels(map: Map<Point, Tile>): Map<Point, Tile> {
    val labeledMap = map.toMutableMap()
    map.filterValues { it is Floor }.forEach {
        val label = findLabel(map, it.key)
        if (label != null) labeledMap[it.key] = Floor(label.first, isInner(map, it.key, label.second))
    }
    return labeledMap
}

private fun findLabel(map: Map<Point, Tile>, point: Point): Pair<String, Direction>? {
    val direction = Direction.values().firstOrNull {
        map[point.move(it)] is Label
    } ?: return null
    val labelPoint = point.move(direction)
    val letter = map[labelPoint] as Label
    val letter2 = map[labelPoint.move(direction)] as Label
    val label = "${letter.character}${letter2.character}"
    return if (direction in listOf(SOUTH, EAST)) label to direction
    else label.reversed() to direction
}
