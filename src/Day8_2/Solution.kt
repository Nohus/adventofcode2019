package Day8_2

import solveInputs

fun main() {
    solveInputs { input ->
        val width = 25
        val height = 6
        val layerSize = width * height

        val layers = input.chunked(1).chunked(layerSize)
        val image = MutableList(layerSize) { "2" }
        layers.reversed().forEach { layer ->
            layer.forEachIndexed { index, pixel ->
                if (pixel != "2") image[index] = pixel
            }
        }
        image.map { if (it != "0") "█" else " " }.chunked(width).forEach {
            println(it.joinToString(""))
        }
    }
}
