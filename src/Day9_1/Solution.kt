package Day9_1

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        IntCodeComputer(input, 1).run().readNum()
    }
}
