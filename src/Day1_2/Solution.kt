package Day1_2

import solveInputs

fun main() {
    solveInputs { input ->
        input.lines().map(String::toInt).map(::getFuelRequirement).sum()
    }
}

fun getFuelRequirement(mass: Int): Int {
    if (mass <= 6) return 0
    val fuel = (mass / 3) - 2
    return fuel + getFuelRequirement(fuel)
}
