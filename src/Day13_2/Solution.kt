package Day13_2

import intcode.IntCodeComputer
import solveInputs

data class Point(val x: Int, val y: Int)

fun main() {
    solveInputs { input ->
        val screen = mutableMapOf<Point, Int>()
        val computer = IntCodeComputer(input)
        computer[0] = 2
        var score = 0
        while (true) {
            computer.clearInput().write(getJoystickOutput(screen))
            val x = computer.read() ?: break
            val y = computer.read() ?: break
            if (x == -1 && y == 0) {
                score = computer.read() ?: break
            } else {
                val tile = computer.read() ?: break
                screen[Point(x, y)] = tile
            }
        }
        score
    }
}

private fun getJoystickOutput(screen: Map<Point, Int>): Int {
    val paddle = screen.entries.firstOrNull { it.value == 3 }?.key?.x ?: return 0
    val ball = screen.entries.firstOrNull { it.value == 4 }?.key?.x ?: return 0
    return when {
        paddle > ball -> -1
        paddle < ball -> 1
        else -> 0
    }
}

private fun printScreen(screen: Map<Point, Int>) {
    for (y in 0..19) {
        for (x in 0..36) {
            if (screen[Point(x, y)] !in 0..4) return
        }
    }
    for (y in 0..19) {
        for (x in 0..36) {
            val symbol = when (screen[Point(x, y)]) {
                0 -> " "
                1 -> "█"
                2 -> "▣"
                3 -> "▬"
                4 -> "●"
                else -> "?"
            }
            print(symbol)
        }
        println()
    }
}
