package Day16_1

import solveInputs
import kotlin.math.abs

fun main() {
    solveInputs { input ->
        var numbers = input.chunked(1).map(String::toInt)
        repeat(100) {
            numbers = applyPhase(numbers)
        }
        numbers.joinToString("").take(8)
    }
}

private fun applyPhase(numbers: List<Int>): List<Int> {
    return List(numbers.size) {
        numbers.zip(getPattern(it, numbers.size)).map { it.first * it.second }.sum()
    }.map { getDigit(it) }
}

private fun getDigit(number: Int): Int {
    return abs(number) % 10
}

private fun getPattern(index: Int, length: Int): List<Int> {
    val list = listOf(0, 1, 0, -1)
        .map { number -> List(index + 1) { number } }
        .flatten()
    val pattern = list.toMutableList()
    while (pattern.size <= length) pattern.addAll(list)
    return pattern.drop(1).take(length)
}
