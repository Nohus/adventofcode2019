package Day16_2

import solveInputs
import kotlin.math.abs

fun main() {
    solveInputs { input ->
        var numbers = input.chunked(1).map(String::toInt)
        numbers = List(10000) { numbers }.flatten()
        numbers = numbers.drop(input.take(7).toInt())
        repeat(100) {
            numbers = applyFastPhase(numbers)
        }
        numbers.take(8).joinToString("")
    }
}

private fun applyFastPhase(numbers: List<Int>): List<Int> {
    val list = mutableListOf<Int>()
    var rollingSum = 0
    repeat(numbers.size) {
        rollingSum += numbers[numbers.lastIndex - it]
        list += rollingSum
    }
    return list.reversed().map { getDigit(it) }
}

private fun getDigit(number: Int): Int {
    return abs(number) % 10
}
