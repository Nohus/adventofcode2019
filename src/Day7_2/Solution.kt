package Day7_2

import intcode.IntCodeComputer
import intcode.Num
import solveInputs
import java.util.*

fun main() {
    solveInputs { input ->
        val phases = (5..9).toList()
        val amplifiers = List(phases.size) { IntCodeComputer(input) }
        getPermutations(phases).map {
            runAttempt(amplifiers.zip(it))
        }.max()
    }
}

private fun runAttempt(attempt: List<Pair<IntCodeComputer, Int>>): Num {
    attempt.forEach { it.first.reset().write(it.second) }
    var output = Num.ZERO
    while (true) {
        output = attempt[0].first.write(output).readNum() ?: break
        Collections.rotate(attempt, 1)
    }
    return output
}

private fun getPermutations(list: List<Int>): List<List<Int>> {
    return if (list.size == 2) listOf(listOf(list[0], list[1]), listOf(list[1], list[0]))
    else list.map { initial ->
        getPermutations(list - initial).map { permutation -> listOf(initial) + permutation }
    }.flatten()
}
