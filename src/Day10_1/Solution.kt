package Day10_1

import solveInputs

data class Point(val x: Int, val y: Int)

fun main() {
    solveInputs { input ->
        val asteroids = mutableListOf<Point>()
        input.lines().forEachIndexed { y, row ->
            row.forEachIndexed { x, char ->
                if (char == '#') asteroids += Point(x, y)
            }
        }

        asteroids.map { station ->
            asteroids.count { isVisible(station, it, asteroids) }
        }.max()
    }
}

private fun isVisible(a: Point, b: Point, points: List<Point>): Boolean {
    (points - setOf(a, b)).forEach {
        if (isBetween(a, b, it)) return false
    }
    return true
}

private fun isBetween(a: Point, b: Point, c: Point): Boolean {
    val crossProduct = (c.y - a.y) * (b.x - a.x) - (c.x - a.x) * (b.y - a.y)
    if (crossProduct != 0) return false
    val dotProduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y) * (b.y - a.y)
    if (dotProduct < 0) return false
    val squaredLength = (b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y)
    if (dotProduct > squaredLength) return false
    return true
}
