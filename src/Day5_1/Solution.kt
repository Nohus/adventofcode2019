package Day5_1

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        IntCodeComputer(input, 1).run().readAll().last()
    }
}
