package Day21_2

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        IntCodeComputer(input).apply {
            readText()
            write("NOT A J\n") // Hole at 1 -> Jump
            write("NOT B T\n") // Hole at 2 -> Temp
            write("OR T J\n") // Hole at 2 or 1 -> Jump
            write("NOT C T\n") // Hole at 3 -> Temp
            write("OR T J\n") // Hole at 3 or 2 or 1 -> Jump
            write("AND D J\n") // Hole we can jump over -> Jump
            write("NOT E T\n") // Hole after jump -> Temp
            write("AND H T\n") // Hole after jump and ground at second jump -> Temp
            write("OR E T\n") // Can move after jump or jump again -> Temp
            write("AND T J\n") // Hole we can jump over and move forward or jump again -> Jump
            write("RUN\n")
            readText()
            readText()
            readText()
        }.readText()
    }
}
