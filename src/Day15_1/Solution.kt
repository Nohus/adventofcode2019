package Day15_1

import Day15_1.Direction.*
import Day15_1.Status.*
import Day15_1.Tile.*
import intcode.IntCodeComputer
import solveInputs

enum class Direction {
    NORTH, SOUTH, WEST, EAST;

    fun toInt() = when (this) {
        NORTH -> 1
        SOUTH -> 2
        WEST -> 3
        EAST -> 4
    }

    fun other() = when (this) {
        NORTH -> SOUTH
        SOUTH -> NORTH
        WEST -> EAST
        EAST -> WEST
    }
}

data class Point(val x: Int, val y: Int) {

    fun move(direction: Direction) = when (direction) {
        NORTH -> Point(x, y - 1)
        SOUTH -> Point(x, y + 1)
        WEST -> Point(x - 1, y)
        EAST -> Point(x + 1, y)
    }
}

enum class Tile {
    UNKNOWN, FLOOR, WALL, OXYGEN
}

enum class Status {
    HIT, MOVED, REACHED;

    companion object {
        fun fromInt(status: Int) = when (status) {
            0 -> HIT
            1 -> MOVED
            2 -> REACHED
            else -> throw IllegalArgumentException()
        }
    }
}

data class Node(
    val position: Point,
    val previous: Node? = null,
    val directionBack: Direction? = null,
    val unexploredDirection: MutableList<Direction>
) {

    fun move(direction: Direction, map: Map<Point, Tile>): Node {
        return if (direction == directionBack) {
            previous!!
        } else {
            unexploredDirection -= direction
            val newPosition = position.move(direction)
            val directions = listOf(NORTH, SOUTH, EAST, WEST).filter {
                map[newPosition.move(it)] ?: UNKNOWN == UNKNOWN
            }
            Node(newPosition, this, direction.other(), directions.toMutableList())
        }
    }

    fun getDepth(): Int {
        return if (previous == null) 0
        else 1 + previous.getDepth()
    }
}

fun main() {
    solveInputs { input ->
        val computer = IntCodeComputer(input)
        val map = mutableMapOf<Point, Tile>()
        map += Point(0, 0) to FLOOR
        var position = Point(0, 0)
        var node = Node(position, null, null, listOf(NORTH, SOUTH, EAST, WEST).toMutableList())

        loop@ while (true) {
            val direction: Direction = if (node.unexploredDirection.isNotEmpty()) {
                node.unexploredDirection.first()
            } else {
                node.directionBack ?: break
            }
            computer.write(direction.toInt())
            val status = Status.fromInt(computer.read()!!.toInt())
            val newPosition = position.move(direction)
            when (status) {
                HIT -> {
                    map[newPosition] = WALL
                    node.unexploredDirection -= direction
                }
                MOVED -> {
                    map[newPosition] = FLOOR
                    position = newPosition
                    node = node.move(direction, map)
                }
                REACHED -> {
                    node = node.move(direction, map)
                    break@loop
                }
            }
        }
        node.getDepth()
    }
}
