package Day2_1

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        val computer = IntCodeComputer(input)
        computer[1] = 12
        computer[2] = 2
        computer.run()
        computer[0]
    }
}
