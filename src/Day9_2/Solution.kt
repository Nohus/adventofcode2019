package Day9_2

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        IntCodeComputer(input, 2).run().read()
    }
}
