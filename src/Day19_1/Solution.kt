package Day19_1

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        val computer = IntCodeComputer(input)
        var affected = 0
        for (x in 0 until 50) {
            for (y in 0 until 50) {
                affected += computer.reset().write(x, y).read()!!.toInt()
            }
        }
        affected
    }
}
