package Day3_2

import Day3_2.Direction.*
import solveInputs

data class Point(val x: Int, val y: Int) {

    fun move(direction: Direction): Point {
        return when (direction) {
            RIGHT -> Point(x + 1, y)
            LEFT -> Point(x - 1, y)
            UP -> Point(x, y - 1)
            DOWN -> Point(x, y + 1)
        }
    }
}

enum class Direction {
    RIGHT, LEFT, UP, DOWN;

    companion object {
        fun fromCharacter(character: Char): Direction {
            return when (character) {
                'R' -> RIGHT
                'L' -> LEFT
                'U' -> UP
                'D' -> DOWN
                else -> throw IllegalArgumentException()
            }
        }
    }
}

fun main() {
    solveInputs { input ->
        val wire1 = getWirePoints(input.lines()[0])
        val wire2 = getWirePoints(input.lines()[1])
        wire1.filter { it.key in wire2.keys }
            .map { wire1.getValue(it.key) + wire2.getValue(it.key) }
            .min()
            .toString()
    }
}

fun getWirePoints(input: String): Map<Point, Int> {
    val points = mutableMapOf<Point, Int>()
    val turns = input.split(",")
    var position = Point(0, 0)
    var totalDistance = 0
    turns.forEach {
        val direction = Direction.fromCharacter(it[0])
        val distance = it.substring(1).toInt()
        repeat(distance) {
            val newPosition = position.move(direction)
            totalDistance++
            points += newPosition to totalDistance
            position = newPosition
        }
    }
    return points
}
