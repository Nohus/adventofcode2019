package Day2_2

import intcode.IntCodeComputer
import intcode.toNum
import solveInputs

fun main() {
    solveInputs() { input ->
        val computer = IntCodeComputer(input)
        for (noun in 0..99) {
            for (verb in 0..99) {
                computer[1] = noun
                computer[2] = verb
                computer.run()
                if (computer[0] == 19690720.toNum()) return@solveInputs 100 * noun + verb
                computer.reset()
            }
        }
    }
}
