package Day12_1

import solveInputs
import kotlin.math.abs

data class Position(val x: Int, val y: Int, val z: Int) {

    operator fun plus(other: Position): Position {
        return Position(x + other.x, y + other.y, z + other.z)
    }
}
data class Moon(val position: Position, val velocity: Position) {

    fun getEnergy(): Int {
        val potential = abs(position.x) + abs(position.y) + abs(position.z)
        val kinetic = abs(velocity.x) + abs(velocity.y) + abs(velocity.z)
        return potential * kinetic
    }
}

fun main() {
    solveInputs { input ->
        var moons = input.lines().map { line ->
            val components = line.split(", ").map {
                it.substringAfter("=").substringBefore(">").toInt()
            }
            Position(components[0], components[1], components[2])
        }.map { Moon(it, Position(0, 0, 0)) }

        val answer: Int
        var step = 1
        while (true) {
            moons = moons.map { moon ->
                var velocity = moon.velocity
                moons.filterNot { it == moon }.forEach { other ->
                    if (other.position.x > moon.position.x) velocity = velocity.copy(x = velocity.x + 1)
                    if (other.position.x < moon.position.x) velocity = velocity.copy(x = velocity.x - 1)
                    if (other.position.y > moon.position.y) velocity = velocity.copy(y = velocity.y + 1)
                    if (other.position.y < moon.position.y) velocity = velocity.copy(y = velocity.y - 1)
                    if (other.position.z > moon.position.z) velocity = velocity.copy(z = velocity.z + 1)
                    if (other.position.z < moon.position.z) velocity = velocity.copy(z = velocity.z - 1)
                }
                val position = moon.position + velocity
                Moon(position, velocity)
            }
            if (step == 1000) {
                answer = moons.sumBy { it.getEnergy() }
                break
            }
            step++
        }
        answer
    }
}
