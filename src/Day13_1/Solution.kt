package Day13_1

import intcode.IntCodeComputer
import solveInputs

fun main() {
    solveInputs { input ->
        val computer = IntCodeComputer(input)
        var blocks = 0
        while (true) {
            val x = computer.read() ?: break
            val y = computer.read() ?: break
            val tile = computer.read() ?: break
            if (tile == 2) blocks++
        }
        blocks
    }
}
