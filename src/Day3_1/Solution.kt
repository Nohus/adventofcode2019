package Day3_1

import Day3_1.Direction.*
import solveInputs
import kotlin.math.abs

data class Point(val x: Int, val y: Int) {

    fun move(direction: Direction): Point {
        return when (direction) {
            RIGHT -> Point(x + 1, y)
            LEFT -> Point(x - 1, y)
            UP -> Point(x, y - 1)
            DOWN -> Point(x, y + 1)
        }
    }

    fun getManhattanDistance(other: Point): Int {
        return abs(x - other.x) + abs(y + other.y)
    }
}

enum class Direction {
    RIGHT, LEFT, UP, DOWN;

    companion object {
        fun fromCharacter(character: Char): Direction {
            return when (character) {
                'R' -> RIGHT
                'L' -> LEFT
                'U' -> UP
                'D' -> DOWN
                else -> throw IllegalArgumentException()
            }
        }
    }
}

fun main() {
    solveInputs { input ->
        val wire1 = getWirePoints(input.lines()[0])
        val wire2 = getWirePoints(input.lines()[1])
        wire1.filter { it in wire2 }
            .map { Point(0, 0).getManhattanDistance(it) }
            .min()
            .toString()
    }
}

fun getWirePoints(input: String): Set<Point> {
    val points = mutableSetOf<Point>()
    val turns = input.split(",")
    var position = Point(0, 0)
    turns.forEach {
        val direction = Direction.fromCharacter(it[0])
        val distance = it.substring(1).toInt()
        repeat(distance) {
            val newPosition = position.move(direction)
            points += newPosition
            position = newPosition
        }
    }
    return points
}
