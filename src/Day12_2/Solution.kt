package Day12_2

import intcode.Num
import intcode.times
import intcode.toNum
import solveInputs

data class Moon(val position: List<Int>, val velocity: List<Int>)

fun main() {
    solveInputs { input ->
        var moons = input.lines().map { line ->
            line.split(", ").map {
                it.substringAfter("=").substringBefore(">").toInt()
            }
        }.map { Moon(it, List(it.size) { 0 }) }
        val dimensions = moons.first().position.size

        val initial = moons.map { it.copy() }
        val answers = MutableList(dimensions) { -1 }
        val answer: Num
        var step = 1
        while (true) {
            moons = moons.map { moon ->
                var velocity = moon.velocity
                moons.filterNot { it == moon }.forEach { other ->
                    velocity = velocity.mapIndexed { index: Int, velocity: Int ->
                        if (other.position[index] > moon.position[index]) return@mapIndexed velocity + 1
                        if (other.position[index] < moon.position[index]) return@mapIndexed velocity - 1
                        velocity
                    }
                }
                val position = moon.position.mapIndexed { index: Int, position: Int -> position + velocity[index] }
                Moon(position, velocity)
            }
            repeat(dimensions) { dimension ->
                if (answers[dimension] == -1) {
                    if (initial.zip(moons).all {
                            it.first.position[dimension] == it.second.position[dimension] &&
                            it.first.velocity[dimension] == it.second.velocity[dimension]
                        }) {
                        answers[dimension] = step
                    }
                }
            }
            if (answers.all { it != -1 }) {
                answer = answers.map { it.toNum() }.reduce { acc, i -> leastCommonMultiple(acc, i) }
                break
            }
            step++
        }
        answer
    }
}

private fun leastCommonMultiple(a: Num, b: Num): Num {
    return a.times(b).divide(greatestCommonDivisor(a, b))
}

private fun greatestCommonDivisor(numberA: Num, numberB: Num): Num {
    var a = numberA
    var b = numberB
    var remainder: Num
    do {
        remainder = a.remainder(b)
        a = b
        b = remainder
    } while (b != Num.ZERO)
    return a
}
