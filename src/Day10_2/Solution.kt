package Day10_2

import solveInputs
import kotlin.math.atan2

data class Point(val x: Int, val y: Int)

fun main() {
    solveInputs { input ->
        val asteroids = mutableListOf<Point>()
        input.lines().forEachIndexed { y, row ->
            row.forEachIndexed { x, char ->
                if (char == '#') asteroids += Point(x, y)
            }
        }

        val stationWithAsteroids = asteroids.map {
            it to getVisibleFrom(it, asteroids)
        }.maxBy { it.second.size }!!

        val bet = stationWithAsteroids.second.map {
            it to getAngle(stationWithAsteroids.first, it)
        }.sortedBy { it.second }[199].first

        bet.x * 100 + bet.y
    }
}

private fun getAngle(a: Point, b: Point): Double {
    val angle = Math.toDegrees(atan2((b.y - a.y).toDouble(), (b.x - a.x).toDouble())) + 90
    return if (angle >= 0) angle else angle + 360
}

private fun getVisibleFrom(a: Point, points: List<Point>): List<Point> {
    return points.filter { isVisible(a, it, points) }
}

private fun isVisible(a: Point, b: Point, points: List<Point>): Boolean {
    (points - setOf(a, b)).forEach {
        if (isBetween(a, b, it)) return false
    }
    return true
}

private fun isBetween(a: Point, b: Point, c: Point): Boolean {
    val crossProduct = (c.y - a.y) * (b.x - a.x) - (c.x - a.x) * (b.y - a.y)
    if (crossProduct != 0) return false
    val dotProduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y) * (b.y - a.y)
    if (dotProduct < 0) return false
    val squaredLength = (b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y)
    if (dotProduct > squaredLength) return false
    return true
}
